data Exp = Num Integer
          | Mul Exp Exp
          | Plus Exp Exp

instance Show Exp where
  show (Num a) = show a
  show (Plus lhs rhs) = showsPrec 20 lhs ("+" ++ (showsPrec (-20) rhs ""))
  show (Mul  lhs rhs) = showsPrec 10 lhs ("*" ++ (showsPrec (-10) rhs ""))


eval :: Exp -> Integer
eval (Num a) = a
eval (Plus lhs rhs) = (+) (eval $ lhs) (eval $ rhs)
eval (Mul lhs rhs) = (*) (eval $ lhs) (eval $ rhs)

test00 = Mul (Plus (Num 1) (Num 5)) (Num 2)
