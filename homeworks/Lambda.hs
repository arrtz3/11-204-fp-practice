data Term = Var String
            | Abstract String Term
            | App Term Term
            deriving(Eq, Show)
type Context  = [(String, Term)]
eval t = eval' [] t
          where
            eval' context l@(Var a) = maybe l id (lookup a context)
            eval' context (Abstract a b) = Abstract a (eval' context b)
            eval' context (App a b) = apply context(eval' context a)(eval' context b)
            apply context  a b = case a of
                              (Abstract c d) -> eval' ((c, b):context) d
                              otherwise -> App a b


test1 = eval $ App (Abstract "x" (Abstract "x" (Var "x"))) (Abstract "x" (Var "x"))


test2 = aval $ App (Abstract "y" (Abstract "x" (Var "y"))) (Abstract "x" (Var "x"))

test3 = eval $ (App (App (Abstract "x" (Abstract "y" (Var "x"))) (Abstract "a" (Var "a"))) (App (Abstract "x" (App (Var "x") (Var "x"))) (Abstract "x" (App (Var "x") (Var "x")))))


test4 = eval $ App (Abstract "x" (Var "x")) (Var "x")
