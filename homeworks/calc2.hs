data Exp = Num Integer
          | Var String
          | Mul Exp Exp
          | Plus Exp Exp

instance Show Exp where
  show (Num a) = show a
  show (Var var) = var
  show (Plus lhs rhs) = showsPrec 20 lhs ("+" ++ (showsPrec (-20) rhs ""))
  show (Mul  lhs rhs) = showsPrec 10 lhs ("*" ++ (showsPrec (-10) rhs ""))


type Context = [(String, Val)]
data Val = Val Integer
          | Nil

instance Show Val where
  show (Val a) = "Value " ++ (show a)
  show a = "Nil"

deref (Val a) = a

eval :: Context -> Exp -> Val
eval context (Num a) = Val a
eval context (Var var) = maybe Nil id (lookup var context)
eval context (Plus lhs rhs) = Val ((+) (deref ( eval context lhs)) (deref ( eval context rhs)))
eval context (Mul lhs rhs) = Val ((*) (deref ( eval context lhs)) (deref ( eval context rhs)))


context1 = [("x", Val 1), ("y", Val 1)]

test_1 = Mul (Num 2) (Plus (Num 3) (Num 4))  -- 2 * (3 + 4) = Value 14
test_5 = Mul (Plus (Num 2) (Num 3)) (Num 4)  -- (2 + 3) * 4 = Value 20

test_with_context_0 = Mul (test_4) (Var "x")   -- 2*(3+4) * x
test_with_context_1 = Plus (Var "y") (test_5) -- y + (2+3)*4
test_with_context_2 = Mul test_with_context_0 test_with_context_1
test_with_context_3 = eval context1 test_with_context_0 -- 24 * 2 = Value 28
test_with_context_4 = eval context1 test_with_context_1 -- 20 + 1 = Value 21
test_with_context_5 = eval context1 test_with_context_2 -- 28 * 21 = Value 588
