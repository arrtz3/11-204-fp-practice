-- арифметические прогрессии длины 3 с расстоянием k
-- например, take 2 (arith 2) = [[5,11,17], [7,13,19]]

nat = 1 : map (+1) nat
isPrime 1 = False
isPrime 2 = True
isPrime n | even n = False
          | otherwise = not $ any (\i -> n`mod`i == 0) $ takeWhile (\i -> i * i <= n) [3,5..]
primes = filter isPrime nat

isArith :: (Ord a, Num a) => (a, (a, a)) -> Bool
isArith t = num2 - num1 == num3 - num2 where
	num1 = fst t
	num2 = fst (snd t)
	num3 = snd (snd t)

iterWithFilter :: (Ord b, Num b) => [(b, (b, b))] -> [[b]]
iterWithFilter [] = []
iterWithFilter (x:xs) | isArith x = [fst x, fst $ snd x, snd $ snd x]:iterWithFilter xs
							| otherwise = iterWithFilter xs
tailN 0 list = list
tailN n list = tailN (n - 1) (tail list)

arith k = iterWithFilter arr where
	sndList = tailN k primes
	arr = zip primes $ zip sndList (tailN k sndList)

-- [(2,a), (4, b), (6, c), (8, d), (3, e), (1, f), (7, g), (5, h)]
-- положение ферзей на шахматной доске
-- список номеров горизонталей, на которых находятся ферзи
-- например, Board [1,2,3,4,5,6,8,7] -- это такое расположение
--  +--------+
-- 8|      ? |
-- 7|       ?|
-- 6|     ?  |
-- 5|    ?   |
-- 4|   ?    |
-- 3|  ?     |
-- 2| ?      |
-- 1|?       |
-- -+--------+
--  |abcdefgh|

newtype Board = Board { unBoard :: [Int] } deriving (Eq,Show)

queens :: Int -> [[Int]]
queens n = filter test (generate n)
    where generate 0 = [[]]
          generate k = [q : qs | q <- [1..n], qs <- generate (k - 1)]
          test [] = True
          test (q:qs) = isSafe q qs && test qs
          isSafe try qs = not (try `elem` qs || sameDiag try qs)
          sameDiag try qs = any (\(colDist,q) -> abs (try - q) == colDist) $ zip [1..] qs

-- Белые начинают и дают мат в два хода
--
-- Белые: Фf8 g4 Крh2
-- Черные: g5 h5 a4 Крh4
--
-- (написать перебор всех вариантов полуходов длины три,
-- вернуть список последовательностей полуходов, ведущих
-- к решению; до этого определить необходимые типы)
-- См. https://en.wikipedia.org/wiki/Chess_notation

-- Определите так, как нужно

--type Move = Int -- ???

--solutions :: [[Move]]
--solutions = undefined
