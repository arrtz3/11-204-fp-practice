data Types = MyBool
    | Impl Types Types
    deriving (Eq,Show)

data Binding = BaseBind
             | VarBind Types

type Context = [(String, Binding)]

data Term = MyTrue
      | MyFalse
      | MyIf Term Term Term
      | Var String
      | Abstract String Types Term
      | App Term Term
      deriving Show

addBinding :: Context -> String -> Binding -> Context

addBinding context x bind = (x, bind):context

getBinding  :: Context -> String -> Binding

getBinding (x:xs) name | fst x == name = snd x
                       | otherwise = getBinding xs name


-- typeOf' context name = maybe Nothing id (lookup name context)
typeOf :: Term -> Types
typeOf t = typeOf' [] t
          where
          typeOf' context MyTrue = MyBool
          typeOf' context MyFalse = MyBool
          typeOf' context (MyIf a b c)
            | typeOf' context a /= MyBool = error "predicate is not a bool"
            | otherwise = let
              resultB = typeOf' context b
              resultC = typeOf' context c
              in if resultC == resultB
                then resultB
                else error "result in if must be one statment"
          typeOf' context v@(Var a) = let (VarBind b) = getBinding context a
                                    in b
          typeOf' context (Abstract name var body)
            = let context' = addBinding context name $ VarBind var
                  resultBody = typeOf' context' body
              in Impl var  resultBody
          typeOf' context (App a b)
            = let result_a = typeOf' context a
                  result_b = typeOf' context b
              in case result_a of
                (Impl result_impl1 result_impl2) -> if result_b == result_impl1
                                                    then result_impl2
                                                    else error "not equals statment in implication"
                otherwise -> error "must be implication type"

test1 = typeOf (Abstract "x" (Impl MyBool MyBool) (Var "x"))

test2 = typeOf (App (Abstract "x" (Impl MyBool MyBool) (Var "x")) (Abstract "y" MyBool (Var "y")))

test3 = typeOf (Abstract "x" MyBool MyTrue)
