data Variable = Var String
data Factor = Num Integer
              | Varib Variable
              | Expr Expression

instance Show Factor where
  show (Num a) = show a
  show (Var a) = a
  show (Expr a) = "("++ show a + ")"

--                            '*'
data Summand = Mul Factor Summand
--            '/'
              | Div Factor Summand
              | SFactor Factor
              deriving(Eq, Show)

--                              '+'
data Expression = Plus Summand Summand
                -- '-'
                | Div Summand Summand
                | Base Summand
                deriving(Eq, Show)

--                          ':='
data Comand = CAssigment Variable Expression
              deriving(Eq, Show)


instance Eq Comand where
   (+) :: fdsa

--                      comand ';'
data Progam = PSemicolon Comand Progam
--                          '.'
          | PDot Comand
          deriving(Eq, Show)

eval


test1 = Var "x"
test2 = FNumber( Num (1))
test3 = FVariable test1
test4 = SFactor( FVariable ( Var( "x" ) ) )
test5 = SFactor( FNumber ( Num (1) ) )
test6 = SMul test2 test4
test7 = SDiv test3 test5
test8 = ESummand test6 test7
test9 = CAssigment test1  test8
test10 = PDot test9
test11 = PSemicolon test9 test10
