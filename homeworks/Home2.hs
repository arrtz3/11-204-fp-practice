
-- | deletes first arg from second arg
-- >>> deleteInt 1 [1,2,1,3,4]
-- [2,3,4]
--
-- >>> deleteInt 1 [2,3]
-- [2,3]
--
-- >>> deleteInt 1 []
-- []
deleteInt :: Int -> [Int] -> [Int]
deleteInt n x | n > 0 && n <= length(x) = del' n x
	where 
		del' 1 (x : xs)  =  xs
		del' n (x : xs) = del' (n-1) xs              
-- | returns list of indices of first arg in second arg
-- >>> findIndicesInt 1 [1,2,1,3,4]
-- [0,2]
--
-- >>> findIndices 1 [2,3]
-- []
findIndicesInt :: Int -> [Int] -> [Int]
findIndicesInt k mass =  findx 0 mass 
	where
		findx index [] = []
		findx index (x:xs) = if (x == k)
					then index : (findx (index + 1) $ xs)
					else findx (index + 1) $ xs



-- data Color = Color Int Int Int 
	-- (+) :: Color(a,b,c) + Color(a,b,c)


-- | tribo_n = tribo_{n-1} + tribo_{n-2} + tribo_{n-3}
--   tribo_0 = 1
--   tribo_1 = 1
--   tribo_2 = 1
--
-- >>> tribo 0
-- 1
--
-- >>> tribo 1
-- 1
--
-- >>> tribo 2
-- 1
--
-- >>> tribo 3
-- 3
--
-- >>> odd (tribo 100)
-- True
tribo n = undefined

-- Тип Цвет, который может быть Белым, Черным, Красным, Зелёным, Синим, либо Смесь из трёх чисел (0-255)
-- операции сложение :: Цвет -> Цвет -> Цвет
-- (просто складываются интенсивности, если получилось >255, то ставится 255)
-- ПолучитьКрасныйКанал, ПолучитьЗелёныйКанал, ПолучитьСинийКанал :: Цвет -> Int

